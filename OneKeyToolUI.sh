#!/bin/bash

ChangeSwapfile() {
    sudo swapoff -a
    sudo dd if=/dev/zero of=/home/swapfile bs=1G count=$1 status=progress
    sudo mkswap /home/swapfile
    sudo swapon /home/swapfile
}

zen_nospam() {
  zenity 2> >(grep -v 'Gtk' >&2) "$@"
}

zenity --notification --text="作者: 玫瑰与鹿\n有问题群里反馈"
while true; do
    choice=$(zen_nospam --title="自动命令工具" --width=800 --height=600 --list --text="选择功能，第二列为前置功能" --column "功能" --column "前置功能" \
        "设置密码" "无"\
        "更改密码" "无"\
        "关闭只读" "设置密码"\
        "初始化pacman相关功能" "设置密码"\
        "软件商店换源" "无"\
        "安装GE兼容层" "无"\
        "通过pacman安装火狐" "设置密码、初始化pacman相关功能"\
        "安装UU" "设置密码"\
        "安装迅游" "设置密码"\
        "安装Decky插件商店" "设置密码、关闭只读"\
        "安装tomoon" "安装Decky"\
        "调整虚拟内存" "设置密码、关闭只读"\
        "管理远程功能" "设置密码、关闭只读"\
        )
    if [[ $? -eq 1 ]] || [[ $? -eq 5 ]]; then
        exit 1
    fi
    # echo $choice
    case $choice in
        设置密码)
            passwd
            ;;
        更改密码)
            sudo passwd -d deck
            ;;
        1r)
            sudo ~/tools/repair_device.sh chroot
            rm -f /var/lib/overlays/etc/upper/passwd
            rm -f /var/lib/overlays/etc/upper/shadow
            ;;
        关闭只读)
            sudo steamos-readonly disable
            ;;
        初始化pacman相关功能)
            sudo pacman-key --init
            sudo pacman-key --populate archlinux
            ;;
        软件商店换源)
            flatpak remote-modify flathub --url=https://mirror.sjtu.edu.cn/flathub
            ;;
        通过pacman安装火狐)
            sudo pacman -S firefox
            ;;
        安装UU)
            curl -s uudeck.com|sudo sh
            ;;
        安装迅游)
            curl -s sd.xunyou.com|sudo sh
            ;;
        安装Decky插件商店)
            deckysel=$(zen_nospam --title="自动命令工具" --width=600 --height=400 --list --text="需要先进行以下操作\nSteam键->设置->系统->系统设置->打开开发者模式，回到设置向下翻，开发者->打开CEF远程调试" --hide-header --column "方式"\
                "国内源"\
                "官方脚本")
            case $deckysel in
                国内源)
                    curl -L http://dl.ohmydeck.net | sh
                    ;;
                官方脚本)
                    cd /tmp
                    curl -o decysins.sh -sSL https://gitee.com/xjshzys/steamdeck-one-key-tool-s/raw/master/decky_install_script.sh && bash decysins.sh
                    ;;
            esac
            ;;
        安装tomoon)
            curl -L http://i.ohmydeck.net | sh
            ;;
        管理远程功能)
            currentip=$(ip addr show wlan0 | awk '/inet / {print $2}' | awk '{split($1, ip, "/"); print ip[1]}')
            sshsel=$(zen_nospam --title="自动命令工具" --width=600 --height=400 --list --text="远程连接，IP:${currentip}，端口:22，用户名：deck，密码为功能1设置的密码" --column "选项" --column "描述" \
                "安装远程" "启动报错时，安装一次即可"\
                "启动远程" "重启后关闭"\
                "开机自启动" " "\
                "关闭开机自启动" " ")
            case $sshsel in
                安装远程)
                    sudo pacman -S openssh
                    ;;
                启动远程)
                    systemctl start sshd
                    ;;
                开机自启动)
                    sudo systemctl enable sshd
                    sudo systemctl start sshd
                    ;;
                关闭开机自启动)
                    sudo systemctl disable sshd
                    ;;
            esac
            ;;
        dbc)
            curl -s https://gitee.com/cabbage-v50-steamdeck/cabbage-toolkit/raw/master/install/cmdline_installer.sh | bash
            ;;
        bhl)
            curl -s -L https://i.hulu.deckz.fun | sudo HULU_CHANNEL=Preview sh -
            ;;
        调整虚拟内存)
            size=$(zen_nospam --entry --title="自动命令工具" --text="输入需要的虚拟内存大小" --entry-text="9")
            echo $size
            ChangeSwapfile  $size
            ;;
        todesk)
            cd /tmp
            wget https://newdl.todesk.com/linux/todesk_4.1.0_x86_64.pkg.tar.zst
            sudo pacman -U todesk_4.1.0_x86_64.pkg.tar.zst
            ;;
        todeskr)
            sudo systemctl stop todeskd.service
            sudo mv /opt/todesk/config/todeskd.conf /opt/todesk/config/todeskd.conf.bak
            sudo systemctl start todeskd.service
            ;;
        yd)
            sudo efibootmgr -c -L "SteamOS" -l "\EFI\steamos\steamcl.efi" -d /dev/nvme0n1p1
            ;;
        安装GE兼容层)
            curl -o GEInstall.sh -sSL https://gitee.com/xjshzys/steamdeck-one-key-tool-s/raw/master/GEInstall.sh && bash GEInstall.sh
            ;;
        0)
            echo "退出脚本"
            exit 0
            ;;
    esac
done
