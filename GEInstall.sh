#!/bin/bash

GEInstall() {
    protonDir="/home/deck/.steam/root/compatibilitytools.d"

    #if [ -d "$protonDir" ]; then
    #    echo ""
    #else
    #    mkdir -p $protonDir
    #fi

    cdnURL="https://down.npee.cn?"

    # echo $cdnURL$1

    mkdir -p ~/.steam/root/compatibilitytools.d
    echo "开始下载，此版本$3M"
    curl -L $cdnURL$1 --output /home/deck/Desktop/$2
    echo "下载完成，开始解压"
    tar -xf /home/deck/Desktop/$2 -C ~/.steam/root/compatibilitytools.d/
    echo "安装完成"
    rm /home/deck/Desktop/$2
}

GEInstallP() {
    local number=$1

    local f=${number:0:1}
    local r=${number:1}

    local ver="${f}-${r}"

    echo "即将开始下载${ver}"

    GEInstall "https://github.com/GloriousEggroll/proton-ge-custom/releases/download/GE-Proton${ver}/GE-Proton${ver}.tar.gz" "${numver}.tar.gz" "null"

#     echo $ver
}

while true; do
    clear

    echo "****************************************************"
    echo "GE兼容层国内源下载工具，希望能帮助大家减少折腾的痛苦"
    echo ""
    echo "版本: V0.0.2"
    echo "作者: 玫瑰与鹿"
    echo ""
    echo "ps: 使用加速源下载Github中ge的原始仓库，如果失效请反馈，加速源可能不稳定，目前最快速度6MB/s"
    echo "pps: 输入.之前文字安装对应版本（如：输入92，安装9.2版本）"
    echo "ppps: 安装完成后重启机器才能看到兼容层"
    echo "pppps: 输入不在下面列出的版本也可下载（如：输入96，将会安装9.6版本）【必须输入2或3位数字，输入别的会下载失败】"
    echo "****************************************************"
    echo "915. 安装GE9.15"
    echo "914. 安装GE9.14"
    echo "913. 安装GE9.13"
    echo "912. 安装GE9.12"
    echo "911. 安装GE9.11"
    echo "910. 安装GE9.10"
    echo "95. 安装GE9.5"
    echo "94. 安装GE9.4"
    echo "93. 安装GE9.3"
    echo "92. 安装GE9.2"
    echo "91. 安装GE9.1"
    echo "832. 安装GE8.32"
    echo "751d4. 安装GE7.51-暗黑4专用版本"
    echo "0. 退出"

    # 等待用户输入并根据不同的输入执行不同的操作
    read -p "输入需要安装版本号: " choice
    case $choice in
        915)
            GEInstall "https://github.com/GloriousEggroll/proton-ge-custom/releases/download/GE-Proton9-15/GE-Proton9-15.tar.gz" "915.tar.gz" "414"
            ;;
        914)
            GEInstall "https://github.com/GloriousEggroll/proton-ge-custom/releases/download/GE-Proton9-14/GE-Proton9-14.tar.gz" "914.tar.gz" "414"
            ;;
        913)
            GEInstall "https://github.com/GloriousEggroll/proton-ge-custom/releases/download/GE-Proton9-13/GE-Proton9-13.tar.gz" "913.tar.gz" "410"
            ;;
        912)
            GEInstall "https://github.com/GloriousEggroll/proton-ge-custom/releases/download/GE-Proton9-12/GE-Proton9-12.tar.gz" "912.tar.gz" "413"
            ;;
        911)
            GEInstall "https://github.com/GloriousEggroll/proton-ge-custom/releases/download/GE-Proton9-11/GE-Proton9-11.tar.gz" "911.tar.gz" "413"
            ;;
        910)
            GEInstall "https://github.com/GloriousEggroll/proton-ge-custom/releases/download/GE-Proton9-10/GE-Proton9-10.tar.gz" "910.tar.gz" "413"
            ;;
        95)
            GEInstall "https://github.com/GloriousEggroll/proton-ge-custom/releases/download/GE-Proton9-5/GE-Proton9-5.tar.gz" "95.tar.gz" "415"
            ;;
        94)
            GEInstall "https://github.com/GloriousEggroll/proton-ge-custom/releases/download/GE-Proton9-4/GE-Proton9-4.tar.gz" "94.tar.gz" "415"
            ;;
        93)
            GEInstall "https://github.com/GloriousEggroll/proton-ge-custom/releases/download/GE-Proton9-3/GE-Proton9-3.tar.gz" "93.tar.gz" "415"
            ;;
        92)
            GEInstall "https://github.com/GloriousEggroll/proton-ge-custom/releases/download/GE-Proton9-2/GE-Proton9-2.tar.gz" "92.tar.gz" "412"
            ;;
        91)
            GEInstall "https://github.com/GloriousEggroll/proton-ge-custom/releases/download/GE-Proton9-1/GE-Proton9-1.tar.gz" "91.tar.gz" "415"
            ;;
        832)
            GEInstall "https://github.com/GloriousEggroll/proton-ge-custom/releases/download/GE-Proton8-32/GE-Proton8-32.tar.gz" "832.tar.gz" "427"
            ;;
        751d4)
            GEInstall "https://github.com/GloriousEggroll/proton-ge-custom/releases/download/GE-Proton7-51-diablo_4_beta/GE-Proton7-51-diablo_4_beta.tar.gz" "751d4.tar.gz" "395"
            ;;
        0)
            echo "退出脚本"
            exit 0
            ;;
        *)
            GEInstallP $choice
#             echo $choice
#             echo "没有这条指令，重新输入"
            ;;
    esac
    read -n1 -r -p "执行完成，按下任意按键继续" key
done
