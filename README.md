# 一些FAQ
## 小白扫盲
### 第一次开机卡下载界面？
语言选择英语（非必须？待测试），电脑下载UU加速器，搜索deck，按照提示进行操作，大概10-20分钟即可下载完成
### 很多教程里提到的密码到底是什么密码？
本机的管理员密码，等同于Windows的开机密码
### 管理员密码忘记了怎么办？
下载恢复镜像，使用**自动命令**工具中的**重置密码**清除管理员密码
### 管理员密码有什么用？
一些指令需要使用管理员权限运行（指令中的sudo）（等同于Windows的右键-以管理员身份运行），使用sudo需要管理员密码
### 日版/欧版/美版/港版机器有什么区别？
机器本体没有任何区别，仅充电器插头有区别
### 手机充电器能不能给deck充电？什么样的充电器/充电宝能给deck充电？
铭牌上写有15V-3A的就可以给deck充电。其中15V-XXA为PD协议，15V*3A=45W为充电功率，输出为15V-3A的充电器即可满速充电，小于45W可以慢速充电。**不用担心65W/100W伤机器，也不用担心非原装伤机器**。***别用杂牌！别用杂牌！别用杂牌！***
### deck能不能玩xxx游戏？deck玩xxx游戏体验怎么样？
打开哔哩哔哩，搜索deck+游戏名（如：deck 龙之信条2）
### deck能不能干xxx？
deck原装系统为SteamOS，是Linux系统，桌面模式等同于装了Linux的笔记本，装了Windows等同于普通笔记本。总的来说deck可以当做笔记本看待，笔记本能做的deck都能做，不要用NS/PS/XBOX的思维看待deck
### SteamOS和Windows哪个好？有没有必要双系统？
SteamOS对正版游戏友好，基本可以做到下载即玩，大部分补丁可以直接用SteamOS安装。如果有黄油、网游、学习版游戏的需求，建议双系统
## 自动命令工具相关
### 自动命令工具怎么用？
拷贝到**DECK的桌面模式**下，双击运行
### 没有U盘，拷不进去怎么办？
桌面模式——开始菜单——All applications——Konsole，输入
```curl -o OneKeyTool.sh -sSL https://gitee.com/xjshzys/steamdeck-one-key-tool-s/raw/master/OneKeyTool.sh && bash OneKeyTool.sh```
## 双系统相关
### 如何在Windows下用手柄玩游戏
<https://github.com/ayufan/steam-deck-tools/releases/tag/0.6.17>
软件可以把手柄映射成Xbox手柄，或者映射成Steam控制器（可以像OS一样使用Steam官方布局）。除此之外还有风扇控制、性能控制等功能。
### 双系统共享盘，OS下只能读取共享盘，无法写入？
关闭Windows快速启动（方法见百度），并且关闭Windows时使用开始菜单-电源-关机。
## 常用网址
### 游戏画质该怎么配置？
<https://steamdeckhq.com/game-settings/>
### Windows驱动
<https://help.steampowered.com/zh-cn/faqs/view/6121-ECCD-D643-BAA8>
### Steam OS恢复镜像
<https://help.steampowered.com/zh-cn/faqs/view/1B71-EDF2-EB6D-2BB3>
### 着色器及兼容层移动
<https://www.xiaohongshu.com/discovery/item/63722c2400000000230188e1>
### SD专用增强版Win11
<https://www.bilibili.com/read/mobile?id=22833238>
## 杂项
### Emudeck官方的下载文件没有速度？
需要魔法上网
或者
从<https://github.com/EmuDeck/emudeck-electron/releases/download/v2.1.4/EmuDeck-2.1.4.AppImage>下载，拷贝到桌面运行
### GE兼容层下载没有速度？
在<https://github.com/GloriousEggroll/proton-ge-custom/releases>中下载GE-ProtonX-X.tar.gz（自己需要的版本），解压到/Home/.local/share/Steam/compatibilitytools.d中
