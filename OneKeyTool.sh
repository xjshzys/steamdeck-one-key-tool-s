#!/bin/bash
#read -n1 -r -p "按下任意按键进入工具";
#echo ""
ChangeSwapfile() {
    sudo swapoff -a
    sudo dd if=/dev/zero of=/home/swapfile bs=1G count=$1 status=progress
    sudo mkswap /home/swapfile
    sudo swapon /home/swapfile
}

while true; do
    clear

    echo "****************************************************"
    echo "deck命令小工具，希望能帮助大家减少折腾的痛苦"
    echo ""
    echo "版本: V0.1.1"
    echo "作者: 玫瑰与鹿"
    echo ""
    echo "ps: 命令后面方括号中为前置指令"
    echo "pps: 脚本会持续更新，大家有需要可以提"
    echo "ppps: 新手上路，可能会有一些奇怪的问题，欢迎反馈"
    echo "pppps: 指令运行完可能会有一条英文提示，最好翻译一下，不确定的可以群里反馈"
    echo "****************************************************"
    echo "1. 设置密码"
    echo "1d. 移除密码"
    echo "1r. 重置密码（注意：只能在恢复镜像中使用！！！）"
    echo "2. 关闭只读[1]"
    echo "3. 初始化pacman相关功能[1]"
    echo "4. 商店换源(解决自带商店打不开/下载慢)"
    echo "ge. 安装GE兼容层"
    echo "5. 通过pacman安装火狐(完全版，和商店版应该略有区别)[1/3]"
    echo "6. 安装UU插件[1]"
    echo "6xy. 安装迅游插件[1]"
    echo "7. 安装Decky Loader插件商店[1/2]（Steam键->设置->系统->系统设置->打开开发者模式，回到设置向下翻，开发者->打开CEF远程调试）"
    echo "7gf. 通过官方脚本安装Decky插件商店[1/2]（Steam键->设置->系统->系统设置->打开开发者模式，回到设置向下翻，开发者->打开CEF远程调试）"
    echo "7in. 通过国内源安装Decky插件商店[1/2]（Steam键->设置->系统->系统设置->打开开发者模式，回到设置向下翻，开发者->打开CEF远程调试）"
    echo "7tomoon. 安装Tomoon插件[7]"
    echo "yce. 开机自启动远程服务[8]"
    echo "ycd. 关闭开机自启动远程服务[8]"
    echo "ycs. 单次启动远程服务[8]"
    echo "dbc. 安装大白菜工具箱[2]"
    echo "bhl. 安装宝葫芦工具箱[2]"
    echo "swapn. 调整虚拟内存为指定大小[1/2]"
    echo "todesk. 安装todesk[1/2]"
    echo "todeskr. 修复todesk[todesk]"
    echo "yd. 修复引导[1]"
    echo "0. 退出"

    # 等待用户输入并根据不同的输入执行不同的操作
    read -p "请输入需要执行的编号: " choice
    case $choice in
        1)
            passwd
            ;;
        1d)
            sudo passwd -d deck
            ;;
        1r)
            sudo ~/tools/repair_device.sh chroot
            rm -f /var/lib/overlays/etc/upper/passwd
            rm -f /var/lib/overlays/etc/upper/shadow
            ;;
        2)
            sudo steamos-readonly disable
            ;;
        3)
            sudo pacman-key --init
            sudo pacman-key --populate archlinux
            ;;
        4)
            flatpak remote-modify flathub --url=https://mirror.sjtu.edu.cn/flathub
            ;;
        5)
            sudo pacman -S firefox
            ;;
        6)
            curl -s uudeck.com|sudo sh
            ;;
        6xy)
            curl -s sd.xunyou.com|sudo sh
            ;;
        7)
            curl -L http://dl.ohmydeck.net | sh
            ;;
        7gf)
            cd /tmp
            curl -o decysins.sh -sSL https://gitee.com/xjshzys/steamdeck-one-key-tool-s/raw/master/decky_install_script.sh && bash decysins.sh
            ;;
        7in)
            echo "暂停使用"
            # cd /tmp
            # curl -L https://github.com/SteamDeckHomebrew/decky-loader/Decky/install-release.sh | sh
            ;;
        7tomoon)
            curl -L http://i.ohmydeck.net | sh
            ;;
        8)
            sudo pacman -S openssh
            ;;
        yce)
            sudo systemctl enable sshd
            sudo systemctl start sshd
            ;;
        ycd)
            sudo systemctl disable sshd
            ;;
        ycs)
            systemctl start sshd
            echo "远程服务已启动，PC（同局域网）使用winscp进行连接"
            ip addr show wlan0 | awk '/inet / {print $2}' | awk 'NR==1{split($1, ip, "/"); print "IP:" ip[1] ", 端口:22, 用户名:deck, 密码:步骤1设置的密码" }'
            ;;
        dbc)
            curl -s https://gitee.com/cabbage-v50-steamdeck/cabbage-toolkit/raw/master/install/cmdline_installer.sh | bash
            ;;
        bhl)
            curl -s -L https://i.hulu.deckz.fun | sudo HULU_CHANNEL=Preview sh -
            ;;
        swapn)
            read -p "请输入需要更改的虚拟内存大小（单位：G): " size
            ChangeSwapfile $size
            ;;
        todesk)
            cd /tmp
            wget https://newdl.todesk.com/linux/todesk_4.1.0_x86_64.pkg.tar.zst
            sudo pacman -U todesk_4.1.0_x86_64.pkg.tar.zst
            ;;
        todeskr)
            sudo systemctl stop todeskd.service
            sudo mv /opt/todesk/config/todeskd.conf /opt/todesk/config/todeskd.conf.bak
            sudo systemctl start todeskd.service
            ;;
        yd)
            sudo efibootmgr -c -L "SteamOS" -l "\EFI\steamos\steamcl.efi" -d /dev/nvme0n1p1
            ;;
        ge)
            curl -o GEInstall.sh -sSL https://gitee.com/xjshzys/steamdeck-one-key-tool-s/raw/master/GEInstall.sh && bash GEInstall.sh
            ;;
        0)
            echo "退出脚本"
            exit 0
            ;;
        *)
            echo $choice
            echo "没有这条指令，重新输入"
            ;;
    esac
    read -n1 -r -p "执行完成，按下任意按键继续" key
done